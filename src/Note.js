import React, { useState, useEffect  } from 'react';
import './Note.css';


export default function Note() 
{
  
  const [data, setData] = useState([]);
  const [id, setId] = useState(0);


   function textChange(id,event) {

      setData(
        data.map(dt => 
            dt.id === id
            ? {id: dt.id, title: dt.title, text : event.target.value} 
            : dt 
    ));

  }

  function textTitle(id,event) {

    setData(
      data.map(dt => 
          dt.id === id
          ? {id: dt.id, title:  event.target.value, text : dt.text} 
          : dt 
    ));

 }


function save(){
  
  if (typeof(Storage) !== "undefined") 
  {
      localStorage.setItem("myNote", JSON.stringify(data));
  }
  else  { 
          alert("Your browser does not support localStorage");
        }
}


  function add() 
  {
           setData([{title: '', text: '', "id": id} ,...data]);
           setId(id+1);
  }

  function dele(idx)
  {
      let noteData = [...data];

      noteData =  data.filter((dt)=> { return dt.id !== idx});
     
      localStorage.setItem("myNote", JSON.stringify(noteData));
      setData(noteData);
      
  }

 

useEffect(() => {
  
let noteData;

  if (typeof(Storage) !== "undefined") {
   
    if (localStorage.getItem("myNote")) {
           noteData = JSON.parse(localStorage.getItem("myNote"));
    } 
    
  } 

  if(noteData !== undefined && noteData.length > 0) { 
      const maxId = Math.max(...noteData.map(note => note.id), 0);
    
      setData(noteData)
      setId(maxId+1);
  }
  else {
          setData([{title: '', text: '', "id": 0}]);
          setId(1);
  }  

},[setData]);



  return (
    <div>

    <ul className="sticky">
      { 
        data.map(dt => (<li id={dt.id} key={dt.id}> 
                          <div>
                             <img className="add" onClick={add} alt="add note"></img>
                             <img className="save" onClick={save} alt="save"></img>
                             <img className="del"  onClick={()=>dele(dt.id)} alt="delete note"></img>
                             <input className="title" type="text" value={dt.title}  onChange={(e) => textTitle(dt.id,e)} maxLength="17" placeholder="title..."></input>
                             <textarea value={dt.text} onChange={(e) => textChange(dt.id,e)} placeholder="text..."></textarea>
                          </div>
                        </li>))}


    </ul>
    </div>
  );
}